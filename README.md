**orq** is a tiny command-line process orchestrator.

orq runs multiple commands in parallel and interleaves & tags all their output,
so you can easily see in what order outputs happen. For instance, you may want
to run a web server alongside a database or run two batch jobs in parallel.

![Running wget downloads in parallel](demo.gif)


# Usage

`orq` runs a list of shell commands in parallel and interleaves their command line
output

    orq [<file>]

`<file>` should contain a list of commands to be executed. If no file is
specified, the commands are read from stdin instead.


# Examples

We may run two servers in parallel:

    $ orq
    python -m http.server
    node index.js
    node index.js       | Example app listening on port 3000!
    python -m http.serv…| Serving HTTP on 0.0.0.0 port 8000 (http://0.0.0.0:8000/) ...

You can also pipe in commands:

    $ echo "echo foo\necho bar" | orq
    echo foo            | foo
    echo bar            | bar
    echo bar exited with status 0
    echo foo exited with status 0

Alternatively, we can write a file `commands`

    echo foo
    echo bar

and run orq with this file as an argument:

    $ orq commands
    echo foo            | foo
    echo bar            | bar
    echo bar exited with status 0
    echo foo exited with status 0
