use crate::Line;
use failure::Error;
use std::process::ExitStatus;
use termcolor::{Color, ColorChoice, ColorSpec, StandardStream, WriteColor};

pub fn stdout() -> StandardStream {
    StandardStream::stdout(ColorChoice::Always)
}

pub fn stderr() -> StandardStream {
    StandardStream::stderr(ColorChoice::Always)
}

pub fn line(mut writer: impl WriteColor, line: &Line) -> Result<(), Error> {
    writer.set_color(ColorSpec::new().set_fg(Some(line.color)))?;
    write!(&mut writer, "{:20}", limit_text(20, &line.label))?;
    writer.set_color(&ColorSpec::new())?;
    writeln!(&mut writer, "| {}", line.content)?;
    Ok(())
}

fn limit_text(size: usize, text: &str) -> String {
    if text.len() <= size {
        text.to_string()
    } else {
        text[0..size - 1].to_string() + "…"
    }
}

pub fn exit_status(
    mut writer: impl WriteColor,
    label: &str,
    status: ExitStatus,
) -> Result<(), Error> {
    if let Some(code) = status.code() {
        if code != 0 {
            writer.set_color(ColorSpec::new().set_fg(Some(Color::Red)))?;
        }
        writeln!(writer, "{} exited with status {}", label, code)?;
        writer.set_color(&ColorSpec::new())?;
    } else {
        writeln!(writer, "{} terminated by signal", label)?;
    }
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn does_not_limit_fitting_text() {
        assert_eq!(limit_text(5, "uiae"), "uiae");
    }

    #[test]
    fn limits_too_large_text_with_ellipsis() {
        assert_eq!(limit_text(3, "uiae"), "ui…");
    }
}
