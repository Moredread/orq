use failure::{format_err, Error};
use input_parser::{read_commands, retrieve_source};
use output::{stderr, stdout};
use std::{
    io::Write,
    process::{exit, ExitStatus, Stdio},
};
use termcolor::Color;
use tokio::io::{AsyncBufReadExt, BufReader};
use tokio::process::{Child, Command};
use tokio::stream::{Stream, StreamExt};
use tokio::sync::mpsc::{self, Sender};

mod input_parser;
mod output;

#[tokio::main]
async fn main() {
    let source = match retrieve_source().await {
        Ok(source) => source,
        Err(_) => {
            eprintln!("{}", input_parser::USAGE);
            exit(1)
        }
    };

    let (tx, mut rx) = mpsc::channel(512);

    tokio::spawn(async move {
        let mut child_handles = spawn_children(read_commands(source));
        while let Some(handle) = child_handles.next().await {
            send_child_messages(handle, tx.clone());
        }
    });

    while let Some(message) = rx.recv().await {
        match message {
            Message::ChildExit(label, status) => {
                output::exit_status(stderr(), &label, status).unwrap();
            }
            Message::ChildLine(line) => {
                let descriptor = match line.mode {
                    Mode::Stdout => stdout(),
                    Mode::Stderr => stderr(),
                };
                output::line(descriptor, &line).unwrap();
            }
        }
    }
}

fn send_child_messages(mut handle: ChildHandle, mut tx: Sender<Message>) {
    match lines_stream(&mut handle) {
        Ok(mut stream) => {
            let mut tx = tx.clone();
            tokio::spawn(async move {
                while let Some(line) = stream.next().await {
                    let _ = tx.send(Message::ChildLine(line)).await;
                }
            });
        }

        Err(_) => {
            let _ = writeln!(stderr(), "failed to launch child process");
        }
    }

    tokio::spawn(async move {
        if let Ok(status) = handle.child.await {
            let _ = tx
                .send(Message::ChildExit(handle.label.clone(), status))
                .await;
        }
    });
}

#[derive(Clone, Debug)]
pub enum Message {
    ChildLine(Line),
    ChildExit(String, ExitStatus),
}

#[derive(Clone, Debug)]
pub struct Line {
    content: String,
    label: String,
    color: Color,
    mode: Mode,
}

#[derive(Copy, Clone, Debug)]
enum Mode {
    Stdout,
    Stderr,
}

fn lines_stream(
    handle: &mut ChildHandle,
) -> Result<impl Stream<Item = Line> + Send + 'static, Error> {
    let stdout = handle
        .child
        .stdout
        .take()
        .ok_or_else(|| format_err!("missing stdout"))?;
    let stderr = handle
        .child
        .stderr
        .take()
        .ok_or_else(|| format_err!("missing stderr"))?;
    let color = handle.color;
    let label = handle.label.clone();
    let stdout_lines = BufReader::new(stdout)
        .lines()
        .filter_map(Result::ok)
        .map(move |content| Line {
            content,
            color,
            label: label.clone(),
            mode: Mode::Stdout,
        });
    let label = handle.label.clone();
    let stderr_lines = BufReader::new(stderr)
        .lines()
        .filter_map(Result::ok)
        .map(move |content| Line {
            content,
            color,
            label: label.clone(),
            mode: Mode::Stderr,
        });
    Ok(stdout_lines.merge(stderr_lines))
}

struct ChildHandle {
    child: Child,
    label: String,
    color: Color,
}

fn spawn_children(input: impl Stream<Item = String>) -> impl Stream<Item = ChildHandle> {
    let mut index = 0;
    input.filter_map(move |line| {
        if line.trim().is_empty() {
            return None;
        }
        let child = spawn_child(&line).ok()?;
        index += 1;
        Some(ChildHandle {
            child,
            label: line,
            color: color_by_index(index),
        })
    })
}

fn color_by_index(index: usize) -> Color {
    const PRIME: usize = 211;
    const SEED: usize = 149_475;
    let hash = division_hash(PRIME, index + SEED);
    Color::Ansi256(hash as u8 + 19)
}

fn division_hash(prime: usize, value: usize) -> usize {
    value * (value + 3) % prime
}

fn spawn_child(command: &str) -> Result<Child, Error> {
    Command::new("sh")
        .arg("-c")
        .arg(command)
        .stdout(Stdio::piped())
        .stderr(Stdio::piped())
        .stdin(Stdio::piped())
        .spawn()
        .map_err(Error::from)
}
